@extends('layouts.app')

@section('content')
  <div class="container">
      <h2>Products</h2>
      <form method="post" action="{{url('/save_product')}}">
     {{ csrf_field() }}
        <div class="form-group">
          <label>Product name</label>
          <input type="text" name="product_name" id="product_name">
        </div>
        <div class="form-group">
          <label>Quantity in Stock</label>
          <input type="text" name="quantity" id="quantity">
        </div>
        <div class="form-group">
          <label>Price per item</label>
          <input type="text" name="price" id="price">
        </div>
        <div class="form-group">
          <button type="button"   id="submit" >Save</button>
        </div>
      </form>
      <div id="table_div">


        </tbody>
      </table>
    </div>
    </div>
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
          crossorigin="anonymous">
    </script>
  <script>
    $(document).ready(function(){
      $.ajax({
          type: 'GET',
          url: '/products',
          data: {'_token': $('meta[name="csrf-token"]').attr('content')},
           success: function(products){
             // var products = JSON.parse(data);
              var total_product_value = 0;
              var total_per_product = 0;
              var i = 0;
              var table_body = '<table id="products_table" class="table table-striped"> <thead><tr><th>Product Name</th><th>Quantity in Stock</th><th>Price Per Item</th><th>Datetime Submitted</th><th>Total Value Number</th><th>Action</th> </tr></thead> <tbody>'
              for(a in products) {
               // for(j = 0;j<products.length; j++) {
                  total_per_product = products[i]['price'] * products[i]['quantity'];
                  total_product_value = total_product_value + total_per_product;
                  var product_id = products[i]['id'];
                  table_body+='<tr>';
                    table_body+='<td>';
                    table_body+=products[i]['product_name'];
                    table_body+='</td>';
                    table_body+='<td>';
                    table_body+=products[i]['quantity'];
                    table_body+='</td>';
                    table_body+='<td>';
                    table_body+=products[i]['price'];
                    table_body+='</td>';
                    table_body+='<td>';
                    table_body+=products[i]['date_updated'];
                    table_body+='</td>'
                    table_body+='<td>';
                    table_body+=total_per_product;
                    table_body+='</td>';
                    table_body+='<td>';
                    table_body+='<a href="{{url("/edit_product")}}/'+product_id+'">Edit</a>';
                    table_body+='</td>';
                    table_body+='</tr>';
             //   }
                i++;
              }
              table_body+='<tr><td>Total</td><td></td><td></td><td></td>';
              table_body+='<td>';
              table_body+=total_product_value;
              table_body+='</td><td></td></tr></tbody></table>';
              $("#table_div").html(table_body);


           }

      });

      $("#submit").click(function(){
        //get the data.
        var product_name = $('#product_name').val();
        var quantity= $('#quantity').val();
        var price = $('#price').val();
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                  });
        $.ajax({
            type: 'POST',
            url: '/save_product',
            data: {
                    product_name : product_name,
                    quantity: quantity,
                    price: price
                  },
             success: function(products){
              // reset form.
              $('#product_name').val('');
              $('#quantity').val('');
               $('#price').val('');
               var total_product_value = 0;
               var total_per_product = 0;
               var i = 0;
               var table_body = '<table id="products_table" class="table table-striped"> <thead><tr><th>Product Name</th><th>Quantity in Stock</th><th>Price Per Item</th><th>Datetime Submitted</th><th>Total Value Number</th><th>Action</th> </tr></thead> <tbody>'
               for(a in products) {
                // for(j = 0;j<products.length; j++) {
                   total_per_product = products[i]['price'] * products[i]['quantity'];
                   total_product_value = total_product_value + total_per_product;
                   var product_id = products[i]['id'];
                   table_body+='<tr>';
                     table_body+='<td>';
                     table_body+=products[i]['product_name'];
                     table_body+='</td>';
                     table_body+='<td>';
                     table_body+=products[i]['quantity'];
                     table_body+='</td>';
                     table_body+='<td>';
                     table_body+=products[i]['price'];
                     table_body+='</td>';
                     table_body+='<td>';
                     table_body+=products[i]['date_updated'];
                     table_body+='</td>'
                     table_body+='<td>';
                     table_body+=total_per_product;
                     table_body+='</td>';
                     table_body+='<td>';
                     table_body+='<a href="{{url("/edit_product")}}/'+product_id+'">Edit</a>';
                     table_body+='</td>';
                     table_body+='</tr>';
              //   }
                 i++;
               }
               table_body+='<tr><td>Total</td><td></td><td></td><td></td>';
               table_body+='<td>';
               table_body+=total_product_value;
               table_body+='</td><td></td></tr></tbody></table>';
               $("#table_div").html(table_body);


             }

        });
      })


    });
  </script>
@endsection('content')
