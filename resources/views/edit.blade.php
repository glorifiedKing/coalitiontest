@extends('layouts.app')

@section('content')
  <div class="container">
      <h2>Edit Product: {{$product['product_name']}} <a class="btn btn-danger" onclick="return confirm('Are You Sure you want to delete this product?')" href="{{route('products.delete',$product['id'])}}">Delete</a></h2>
      <form method="post" action="{{route('products.update',$product['id'])}}">
     {{ csrf_field() }}
        <div class="form-group">
          <label>Product name</label>
          <input type="text" name="product_name" id="product_name" value="{{$product['product_name']}}">
        </div>
        <div class="form-group">
          <label>Quantity in Stock</label>
          <input type="text" name="quantity" id="quantity" value="{{$product['quantity']}}">
        </div>
        <div class="form-group">
          <label>Price per item</label>
          <input type="text" name="price" id="price" value="{{$product['price']}}">
        </div>
        <div class="form-group">
          <button  id="submit" >Update</button>
        </div>
      </form>

    </div>
@endsection
