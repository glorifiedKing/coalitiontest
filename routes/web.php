<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('products');
});
Route::get('/products', 'ProductController@index')->name('products.index');
Route::get('/edit_product/{id}', 'ProductController@show')->name('products.view');
Route::post('/edit_product/{id}', 'ProductController@update')->name('products.update');
Route::get('/delete_product/{id}', 'ProductController@destroy')->name('products.delete');
Route::post('/save_product', 'ProductController@store')->name('products.store');
Auth::routes();
