<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Exception,Log,File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all products
        $allProducts = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json')) : [];
        return $allProducts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created product into json file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = storage_path('app/products.json');
  //      try
  //      {
        $allProducts = Storage::disk('local')->exists('products.json') ? Storage::disk('local')->get('products.json') : [];
          $allProductsArray = json_decode($allProducts,true);
          $input = $request->only(['product_name','quantity','price']);
          $input['date_updated'] = date('Y-m-d H:i:s');
          $input['id'] = uniqid();
          array_push($allProductsArray,$input);
          Storage::disk('local')->put('products.json',json_encode($allProductsArray));
      //    Storage::disk('local')->append('products.json',json_encode($input));
          //return the products..
          $updatedProducts = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json')) : [];
          return $updatedProducts;
  //      }
  /*      catch(Exception $e)
        {
          return ['error' => 'error','message' => $e->getMessage()];
        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //first get all products from file
        $allProducts = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json'),true) : [];
        //search for id from products.
        $key = array_search($id, array_column($allProducts, 'id'));
        $product = array();
        if($key != FALSE)
        {
          $product['product_name'] = $allProducts[$key]['product_name'];
          $product['quantity'] = $allProducts[$key]['quantity'];
          $product['price'] = $allProducts[$key]['price'];
          $product['id'] = $allProducts[$key]['id'];


        }
        return view('edit',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $allProducts = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json'),true) : [];
      $key = array_search($id, array_column($allProducts, 'id'));
      $product = array();
      if($key != FALSE)
      {
          $allProducts[$key]['product_name'] = $request->product_name;
          $allProducts[$key]['quantity'] = $request->quantity;
          $allProducts[$key]['price'] = $request->price;
          Storage::disk('local')->put('products.json',json_encode($allProducts));


      }
      return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $allProducts = Storage::disk('local')->exists('products.json') ? json_decode(Storage::disk('local')->get('products.json'),true) : [];
      $key = array_search($id, array_column($allProducts, 'id'));

      if($key != FALSE)
      {
          unset($allProducts[$key]);
          Storage::disk('local')->put('products.json',json_encode($allProducts));


      }
      return redirect('/');

    }
}
